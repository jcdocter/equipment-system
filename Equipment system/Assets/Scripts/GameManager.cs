using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private PlayerControls playerControls;
    private InputAction exit;

    private void Awake()
    {
        playerControls = new PlayerControls();
    }

    private void Start()
    {
        exit.performed += _ => Quit();
    }

    private void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnEnable()
    {
        exit = playerControls.Player.Exit;
        exit.Enable();
    }

    private void OnDisable()
    {
        exit.Disable();
    }
}
