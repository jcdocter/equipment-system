using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// idea to change functions of a button
public interface ICommand
{
    void Execute();
}
