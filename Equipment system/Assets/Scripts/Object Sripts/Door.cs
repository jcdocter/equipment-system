using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Transform frontCollider;
    public Transform backCollider;
    public Material greenMaterial;
    public Material redMaterial;
    private Animator animator;

    private bool isOpenFront;
    private bool isOpenBack;
    private bool walkedOver;
    private int entered;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        OpenDoorFront();

        OpenDoorBack();
    }

    private void OpenDoorFront()
    {
        Collider[] checkRange = Physics.OverlapBox(frontCollider.position, frontCollider.localScale, Quaternion.identity);
        foreach (Collider c in checkRange)
        {
            if (c.Equals(FindObjectOfType<CharacterController>()))
            {
                if (!isOpenBack)
                {
                    frontCollider.GetComponent<MeshRenderer>().material = redMaterial;
                    animator.SetBool("OpenFront", true);
                    isOpenFront = true;
                    entered++;
                }
                else
                {
                    animator.SetBool("OpenBack", false);
                    walkedOver = true;
                }

                return;
            }
            if (walkedOver)
            {
                entered = 0;
                walkedOver = false;
            }
        }

        if (entered <= 0)
        {
            isOpenBack = false;
            backCollider.GetComponent<MeshRenderer>().material = greenMaterial;
        }
    }

    private void OpenDoorBack()
    {
        Collider[] checkRange = Physics.OverlapBox(backCollider.position, backCollider.localScale, Quaternion.identity);

        foreach (Collider c in checkRange)
        {
            if (c.Equals(FindObjectOfType<CharacterController>()))
            {
                if (!isOpenFront)
                {
                    backCollider.GetComponent<MeshRenderer>().material = redMaterial;
                    animator.SetBool("OpenBack", true);
                    isOpenBack = true;
                    entered++;
                }
                else
                {
                    animator.SetBool("OpenFront", false);
                    walkedOver = true;
                }

                return;
            }

            if (walkedOver)
            {
                entered = 0;
                walkedOver = false;
            }
        }

        if(entered <= 0)
        {
            isOpenFront = false;
            frontCollider.GetComponent<MeshRenderer>().material = greenMaterial;
        }
    }
}
