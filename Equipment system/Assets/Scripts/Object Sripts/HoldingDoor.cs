using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

// not being used
public class HoldingDoor : MonoBehaviour
{
    public float radius;
    public Transform door;

    private Rigidbody doorBody;
    private PlayerControls playerControls;
    private InputAction hold;

    private GameObject handObject;
    private bool isHolding;
 
    private void Awake()
    {
        doorBody = GetComponent<Rigidbody>();
        playerControls = new PlayerControls();
    }

    private void FixedUpdate()
    {
        if (CheckHand())
        {
            isHolding = hold.ReadValue<float>() > 0.1f;

            if (isHolding)
            {
                door.position = handObject.transform.position;
                doorBody.MovePosition(door.position);
            }
        }
    }

    private bool CheckHand()
    {
        Collider[] checkRange = Physics.OverlapSphere(door.position, radius);

        if (checkRange.Length != 0)
        {
            handObject = checkRange[0].gameObject;
            return true;
        }
        else
        {
            handObject = null;
            return false;
        }
    }


    private void OnEnable()
    {
        hold = playerControls.Environment.Holding;
        hold.Enable();
    }

    private void OnDisable()
    {
        hold.Disable();
    }
}
