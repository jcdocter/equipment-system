using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Lighter : Pickable
{
    public bool lightOn;
    public GameObject lightObject;

    private void Start()
    {
        lightObject.SetActive(false);

        //could add a event lambda
        light.performed += TurnOnOff;
    }

    private void TurnOnOff(InputAction.CallbackContext _context)
    {
        if (grab.objectList.Contains(this.gameObject))
        {
            if (lightOn)
            {
                lightOn = false;
                lightObject.SetActive(false);
                Debug.Log("Light is off");
            }
            else
            {
                lightOn = true;
                lightObject.SetActive(true);
                Debug.Log("Light is on");
            }
        }
    }
}
