using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Pickable : MonoBehaviour
{
    protected Grab grab;

    protected InputAction throwObject;
    protected InputAction shoot;
    protected InputAction reload;
    protected InputAction putOn;
    protected InputAction light;
    protected InputAction fireMode;

    private PlayerControls playerControls;

    private void Awake()
    {
        playerControls = new PlayerControls();
        grab = FindObjectOfType<Grab>();
    }

    private void OnEnable()
    {
        throwObject = playerControls.Objects.Throw;
        shoot = playerControls.Objects.Shoot;
        reload = playerControls.Objects.Reload;
        putOn = playerControls.Objects.PutOn;
        light = playerControls.Objects.Light;
        fireMode = playerControls.Objects.FireMode;

        throwObject.Enable();
        shoot.Enable();
        reload.Enable();
        putOn.Enable();
        light.Enable();
        fireMode.Enable();
    }

    private void OnDisable()
    {
        throwObject.Disable();
        shoot.Disable();
        reload.Disable();
        putOn.Disable();
        light.Disable();
        fireMode.Disable();
    }
}
