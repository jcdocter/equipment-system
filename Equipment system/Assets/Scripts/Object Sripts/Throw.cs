using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Throw : Pickable
{
    public float throwForce;

    private void Start()
    {
        //could add a event lambda
        throwObject.performed += ThrowObject;
    }

    private void ThrowObject(InputAction.CallbackContext _context)
    {
        if (grab.objectList.Contains(this.gameObject))
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();

            rigidbody.useGravity = true;
            rigidbody.AddForce(grab.transform.forward * throwForce, ForceMode.VelocityChange);

            grab.objectList.Remove(this.gameObject);
        }
    }
}
