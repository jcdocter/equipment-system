using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reload : Pickable
{
    public int ammoAmount;
    public bool isHolding;

    private void Update()
    {
        if(grab.objectList.Contains(this.gameObject))
        {
            isHolding = true;
        }
        else
        {
            isHolding = false;
        }
    }
}
