using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public enum WeaponMode
{
    SINGLE,
    AUTO
}

//could create a weaponHandler script to call this script

public class Shoot : Pickable
{
    public int maxAmmo;
    public float fireRate;

    private TextMeshProUGUI ammoText;
    private TextMeshProUGUI fireModeText;
    private GameObject fireModeObject;
    private WeaponMode weaponMode;
    private int currentAmmo = 0;

    private bool reloadPressed;
    private bool canChangeMode;
    private bool autoOn;

    private float nextTimeToFire = 0f;
    private string[] modes;

    private void Start()
    {
        modes = Enum.GetNames(typeof(WeaponMode));
        fireModeObject = FindObjectOfType<UIHolder>().fireModeObject;
        fireModeText = FindObjectOfType<UIHolder>().fireModeText;

        ammoText = FindObjectOfType<UIHolder>().ammoText;
    }

    private void Update()
    {
        switch (weaponMode)
        {
            case WeaponMode.AUTO:
                //wanted to change interaction performance from tap to hold
                fireModeText.text = "Auto fire";
                shoot.performed += _ => autoOn = true;
                shoot.canceled += _ => autoOn = false;
                shoot.performed -= SingleFire;
                break;

            default:
                fireModeText.text = "Single fire";
                shoot.performed += SingleFire;
                break;
        }

        ammoText.text = $"{currentAmmo} / {maxAmmo}";

        if (grab.objectList.Contains(this.gameObject))
        {
            AutoFire();

            canChangeMode = fireMode.ReadValue<float>() < 0f;
            if (canChangeMode)
            {
                SwitchMode();
            }

            reloadPressed = reload.ReadValue<float>() > 0.1f;
            if (reloadPressed)
            {
                for (int i = 0; i < grab.objectList.Count; i++)
                {
                    if (grab.objectList[i].GetComponent<Reload>() != null)
                    {
                        ReloadWeapon(grab.objectList[i].gameObject);
                    }
                }
            }

            ammoText.enabled = true;
            fireModeObject.SetActive(true);
        }
        else
        {
            ammoText.enabled = false;
            fireModeObject.SetActive(false);
        }
    }

    private void AutoFire()
    {
        if (currentAmmo > 0 && autoOn)
        {
            if (Time.time >= nextTimeToFire)
            {
                nextTimeToFire = Time.time + 1f / fireRate;
                currentAmmo--;
            }
        }
    }

    private void SingleFire(InputAction.CallbackContext _context)
    {
        if (currentAmmo > 0)
        {
            currentAmmo--;
        }

        autoOn = false;
    }

    //should be in the Reload script
    private void ReloadWeapon(GameObject _ammoClip)
    {
        Reload reload = _ammoClip.GetComponent<Reload>();

        if (reload.isHolding && currentAmmo < maxAmmo)
        {
            currentAmmo += reload.ammoAmount;

            if(currentAmmo >= maxAmmo)
            {
                currentAmmo = maxAmmo;
            }

            grab.objectList.Remove(_ammoClip);
            Destroy(reload.gameObject);
        }
    }

    private void SwitchMode()
    {
        if((int)weaponMode >= modes.Length - 1)
        {
            weaponMode = 0;
        }
        else
        {
            weaponMode += 1;
        }
    }
}
