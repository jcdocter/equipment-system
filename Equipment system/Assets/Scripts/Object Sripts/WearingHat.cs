using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class WearingHat : Pickable
{
    private bool isWearing;

    private void Start()
    {
        //could add a event lambda
        putOn.performed += Equip;
    }

    private void Update()
    {
        if (isWearing)
        {
            transform.position = grab.headPosition.position;
            transform.rotation = grab.headPosition.rotation;

            //want to take it off
            //transform.GetComponent<Collider>().enabled = false;
        }
    }

    private void Equip(InputAction.CallbackContext _context)
    {
        if (grab.objectList.Contains(this.gameObject))
        {
            grab.objectList.Remove(this.gameObject);
            isWearing = true;
        }
    }
}
