using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Grab : MonoBehaviour
{
    public bool canGrab;
    public float rayDistance = 1.3f;

    public List<GameObject> objectList = new List<GameObject>();

    public Transform leftHand;
    public Transform rightHand;
    public Transform headPosition;

    private Transform cam;
    private Image crosshair;

    private GameObject grabableObject;
    private GameObject objectLeft;
    private GameObject objectRight;

    private PlayerControls playerControls;
    private InputAction grabLeft;
    private InputAction grabRight;

    private bool holdObjectLeft;
    private bool holdObjectRight;

    private bool leftPressed;
    private bool rightPressed;

    private int canReleaseLeft;
    private int canReleaseRight;

    private void Awake()
    {
        playerControls = new PlayerControls();
        cam = FindObjectOfType<Camera>().transform;
        crosshair = FindObjectOfType<UIHolder>().crosshair;
    }

    private void Update()
    {
        HoldingObjectLeftHand();
        HoldingObjectRightHand();

        GrabObject();
    }

    private void GrabObject()
    {
        RaycastHit hit;
        Ray ray = new Ray(cam.position, cam.forward);

        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            grabableObject = hit.transform.gameObject;
            Debug.DrawLine(ray.origin, hit.point, Color.green);

            if (grabableObject.GetComponent<Pickable>())
            {
                crosshair.color = new Color32(20, 255, 0, 83);
                if (canReleaseLeft < 1)
                {
                    leftPressed = grabLeft.ReadValue<float>() > 0.1f;

                    if (leftPressed)
                    {
                        canReleaseLeft++;

                        objectLeft = grabableObject;
                        objectList.Add(objectLeft);
                        holdObjectLeft = true;
                    }
                }

                if (canReleaseRight < 1)
                {
                    rightPressed = grabRight.ReadValue<float>() > 0.1f;

                    if (rightPressed)
                    {
                        canReleaseRight++;

                        objectRight = grabableObject;
                        objectList.Add(objectRight);

                        holdObjectRight = true;

                    }
                }
            }
        }
        else
        {
            crosshair.color = new Color32(255, 22, 0, 83);
            grabableObject = null;
        }

        Debug.DrawRay(ray.origin, cam.forward * rayDistance, Color.red);

    }

    private void HoldingObjectLeftHand()
    {
        if (!objectList.Contains(objectLeft))
        {
            ReleaseObjectLeftHand();
        }

        if (canReleaseLeft >= 1)
        {
            leftPressed = grabLeft.ReadValue<float>() > 0.1f;

            if (leftPressed)
            {
                objectLeft.GetComponent<Rigidbody>().useGravity = true;
                ReleaseObjectLeftHand();
            }
        }

        if (holdObjectLeft)
        {
            objectLeft.transform.position = leftHand.position;
            objectLeft.transform.rotation = leftHand.rotation;

            objectLeft.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    private void HoldingObjectRightHand()
    {
        if(!objectList.Contains(objectRight))
        {
            ReleaseObjectRightHand();
        }

        if (canReleaseRight >= 1)
        {
            rightPressed = grabRight.ReadValue<float>() > 0.1f;

            if (rightPressed)
            {
                objectRight.GetComponent<Rigidbody>().useGravity = true;
                ReleaseObjectRightHand();
            }
        }

        if (holdObjectRight)
        {
            objectRight.transform.position = rightHand.position;
            objectRight.transform.rotation = rightHand.rotation;

            objectRight.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    public void ReleaseObjectLeftHand()
    {
        objectList.Remove(objectLeft);
        holdObjectLeft = false;
        canReleaseLeft = 0;
        objectLeft = null;
    }

    public void ReleaseObjectRightHand()
    {
        objectList.Remove(objectRight);
        holdObjectRight = false;
        canReleaseRight = 0;
        objectRight = null;
    }

    private void OnEnable()
    {
        grabLeft = playerControls.Player.GrabLeft;
        grabRight = playerControls.Player.GrabRight;
        grabLeft.Enable();
        grabRight.Enable();
    }

    private void OnDisable()
    {
        grabLeft.Disable();
        grabRight.Disable();
    }
}
