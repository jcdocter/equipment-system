using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MouseLook
{
    private Transform cam;
    private Transform playerBody;
    private PlayerControls playerControls;
    private InputAction look;
    private float sensitivity;

    private float xRotation;

    public MouseLook(Transform _cam, Transform _playerBody, PlayerControls _playerControls, float _mouseSensitivity)
    {
        this.cam = _cam;
        this.playerBody = _playerBody;
        this.sensitivity = _mouseSensitivity;
        this.playerControls = _playerControls;
    }

    public void EnableLooking()
    {
        look = playerControls.Player.Look;
        look.Enable();
    }

    public void DisableLooking()
    {
        look.Disable();
    }

    public void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void LookAround()
    {
        float mouseX = look.ReadValue<Vector2>().x * sensitivity * Time.deltaTime;
        float mouseY = look.ReadValue<Vector2>().y * sensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        cam.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        playerBody.Rotate(Vector3.up, mouseX);
    }
}
