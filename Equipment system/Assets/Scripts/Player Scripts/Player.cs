using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 6f;
    public float gravity = 9.81f;
    public float groundDistance = 0.4f;
    public float mouseSensitivity = 100f;

    public Transform groundCheck;
    public LayerMask groundMask;

    private PlayerMovement playerMovement;
    private MouseLook mouseLook;

    private CharacterController controller;
    private Transform cam;
    private Vector3 velocity;
    private PlayerControls playerControls;

    private void Awake()
    {
        playerControls = new PlayerControls();
        controller = GetComponent<CharacterController>();
        cam = FindObjectOfType<Camera>().transform;

        playerMovement = new PlayerMovement(controller, playerControls, transform, speed);
        mouseLook = new MouseLook(cam, transform, playerControls, mouseSensitivity);
    }

    private void Start()
    {
        mouseLook.LockCursor();
    }

    private void Update()
    {
        playerMovement.Movement();
        playerMovement.Gravity(velocity, groundCheck, groundMask, gravity, groundDistance);

        velocity.y = playerMovement.GetVelocity().y;
    }

    private void FixedUpdate()
    {
        mouseLook.LookAround();
    }

    private void OnEnable()
    {
        playerMovement.EnableMovement();
        mouseLook.EnableLooking();
    }

    private void OnDisable()
    {
        playerMovement.DisableMovement();
        mouseLook.DisableLooking();
    }
}
