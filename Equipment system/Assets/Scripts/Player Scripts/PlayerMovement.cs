using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement
{
    private Vector3 velocity;
    private Transform playerTransform;
    private CharacterController controller;
    private PlayerControls playerControls;
    private InputAction movement;

    private float speed;
    private bool isGrounded;

    public PlayerMovement(CharacterController _controller, PlayerControls _playerControls, Transform _playerTransform, float _speed)
    {
        this.controller = _controller;
        this.playerTransform = _playerTransform;
        this.speed = _speed;
        this.playerControls = _playerControls;
    }

    public void EnableMovement()
    {
        movement = playerControls.Player.Move;
        movement.Enable();
    }

    public void DisableMovement()
    {
        movement.Disable();
    }

    public void Movement()
    {
        float horizontal = movement.ReadValue<Vector3>().x;
        float vertical = movement.ReadValue<Vector3>().z;

        Vector3 move = playerTransform.right * horizontal + playerTransform.forward * vertical;
        controller.Move(move * speed * Time.deltaTime);
    }

    public void Gravity(Vector3 _velocity, Transform _groundCheck, LayerMask _layerMask, float _gravity, float _radius)
    {
        isGrounded = Physics.CheckSphere(_groundCheck.position, _radius, _layerMask);

        if(isGrounded && _velocity.y < 0)
        {
            return;
        }

        _velocity.y -= _gravity * Time.deltaTime;
        controller.Move(_velocity * Time.deltaTime);

        velocity = _velocity;
    }

    public Vector3 GetVelocity()
    {
        return velocity;
    }
}
