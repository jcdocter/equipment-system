using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIHolder : MonoBehaviour
{
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI fireModeText;
    public Image crosshair;
    public GameObject fireModeObject;
}
